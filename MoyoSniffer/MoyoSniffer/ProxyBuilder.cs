﻿using System.Collections.Generic;
using System.Net;

namespace MoyoSniffer
{
    public class ProxyBuilder
    {
        private readonly IList<string> _proxyList;
        private int _counter = -1;

        public ProxyBuilder(IList<string> proxyList)
        {
            _proxyList = proxyList;
        }

        public IWebProxy CreateProxyFromList()
        {
            _counter++;

            if (_counter > _proxyList.Count - 1)
                _counter = 0;

            return new WebProxy(_proxyList[_counter]);
        }
    }
}
