﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MoyoSniffer.Providers;
using MoyoSniffer.Services.Abstract;

namespace MoyoSniffer
{
    public class SnifferExecutor
    {
        private readonly IRoutesProvider _routesProvider;
        private readonly IProductSearcherService _productSearcherService;
        private readonly IDbCleaner _dbCleaner;

        public SnifferExecutor(IRoutesProvider routesProvider, IProductSearcherService productSearcherService, IDbCleaner dbCleaner)
        {
            _routesProvider = routesProvider;
            _productSearcherService = productSearcherService;
            _dbCleaner = dbCleaner;
        }

        public async Task StartAsync(string[] args)
        {
            if (args.Contains("-wc"))
                await _dbCleaner.ClearNonUniqueValuesAsync();

            if (args.Contains("-wnr"))
            {
                await StartWithNewRouteSearching();

                return;
            }

            var routesFromDb = await _routesProvider.GetRoutesFromDbAsync();

            if (!routesFromDb.Any())
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("No routes in db. Start searching from requests");
                Console.ForegroundColor = ConsoleColor.White;

                await StartWithNewRouteSearching();

                return;
            }

            await _productSearcherService.SearchProductsAsync(routesFromDb);
        }

        private async Task StartWithNewRouteSearching()
        {
            var routesFromRequest = await _routesProvider.GetRoutesFromRequestsAsync();

            await _productSearcherService.SearchProductsAsync(routesFromRequest);
        }
    }
}
