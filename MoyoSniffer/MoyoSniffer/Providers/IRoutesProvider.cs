﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MoyoSniffer.Models;

namespace MoyoSniffer.Providers
{
    public interface IRoutesProvider
    {
        Task<IList<Route>> GetRoutesFromRequestsAsync();
        Task<IList<Route>> GetRoutesFromDbAsync();
    }
}
