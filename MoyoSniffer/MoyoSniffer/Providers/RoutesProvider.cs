﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Configuration;
using MoyoSniffer.Models;
using MoyoSniffer.Services.Abstract;

namespace MoyoSniffer.Providers
{
    public class RoutesProvider : IRoutesProvider
    {
        private readonly ISiteRoutesSearchService _routesSearchService;
        private readonly IConfiguration _configuration;
        private readonly SnifferDbContext _snifferDbContext;

        public RoutesProvider(ISiteRoutesSearchService routesSearchService,
            SnifferDbContext snifferDbContext,
            IConfiguration configuration)
        {
            _routesSearchService = routesSearchService;
            _snifferDbContext = snifferDbContext;
            _configuration = configuration;
        }

        public async Task<IList<Route>> GetRoutesFromRequestsAsync()
        {
            if (int.TryParse(_configuration["ProductSearcherFilter:PriceFrom"], out var priceFrom)
                && int.TryParse(_configuration["ProductSearcherFilter:PriceTo"], out var priceTo))
                return (await _routesSearchService
                    .SearchRoutesFromProductListAsync(priceFrom, priceTo))
                    .Distinct((r1, r2) => r1.Url == r2.Url)
                    .ToList();

            return (await _routesSearchService
                .SearchRoutesFromProductListAsync())
                .Distinct((r1, r2) => r1.Url == r2.Url)
                .ToList();
        }

        public async Task<IList<Route>> GetRoutesFromDbAsync()
        {
            var routeComparer = new RouteComparer();

            if (int.TryParse(_configuration["ProductSearcherFilter:PriceFrom"], out var priceFrom)
                && int.TryParse(_configuration["ProductSearcherFilter:PriceTo"], out var priceTo))
                return (await _snifferDbContext
                    .Routes
                    .Where(route => route.Price >= priceFrom && route.Price <= priceTo)
                    .ToArrayAsync())
                    .Distinct((r1, r2) => r1.Url == r2.Url)
                    .ToList();

            return _snifferDbContext
                .Routes
                .Distinct((r1, r2) => r1.Url == r2.Url)
                .ToList();
        }
    }
}
