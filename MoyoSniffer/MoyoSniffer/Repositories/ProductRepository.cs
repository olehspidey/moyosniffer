﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoyoSniffer.Models;

namespace MoyoSniffer.Repositories
{
    public class ProductRepository : Repository<Product>
    {
        private readonly SnifferDbContext _snifferDbContext;

        public ProductRepository(SnifferDbContext snifferDbContext) : base(snifferDbContext)
        {
            _snifferDbContext = snifferDbContext;
        }

        public override async Task InsertAsync(ICollection<Product> entities)
        {
            try
            {
                var productUrls = entities.Select(product => product.Url).Distinct();
                var productsToDelete = _snifferDbContext.Products.Where(product => productUrls.Contains(product.Url));

                _snifferDbContext
                    .Products
                    .RemoveRange(productsToDelete);
                await _snifferDbContext
                    .Products
                    .AddRangeAsync(entities);
                await _snifferDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Exception in {nameof(InsertAsync)}");
                Console.WriteLine(e);
                Console.WriteLine("Try to insert again");
                await InsertAsync(entities);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Success insert to db");
                Console.ForegroundColor = ConsoleColor.White;
            }
        }
    }
}
