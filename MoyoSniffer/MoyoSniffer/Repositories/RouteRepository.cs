﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoyoSniffer.Models;

namespace MoyoSniffer.Repositories
{
    public class RouteRepository : Repository<Route>
    {
        private readonly SnifferDbContext _snifferDbContext;

        public RouteRepository(SnifferDbContext snifferDbContext) : base(snifferDbContext)
        {
            _snifferDbContext = snifferDbContext;
        }

        public override async Task InsertAsync(ICollection<Route> entities)
        {
            try
            {
                var routesUrls = entities.Select(x => x.Url).Distinct();
                var routesToDelete = _snifferDbContext.Routes.Where(route => routesUrls.Contains(route.Url));

                _snifferDbContext
                    .Routes
                    .RemoveRange(routesToDelete);

                await _snifferDbContext
                    .Routes
                    .AddRangeAsync(entities);
                await _snifferDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Exception in {nameof(InsertAsync)}");
                Console.WriteLine(e);
                Console.WriteLine("Try to insert again");
                await InsertAsync(entities);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Success insert to db");
                Console.ForegroundColor = ConsoleColor.White;
            }
        }
    }
}
