﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoyoSniffer.Models.Abstract;
using MoyoSniffer.Repositories.Abstract;

namespace MoyoSniffer.Repositories
{
    public class Repository<T> : IRepository<T> where T : Entity
    {
        private readonly SnifferDbContext _snifferDbContext;

        public Repository(SnifferDbContext snifferDbContext)
        {
            _snifferDbContext = snifferDbContext;
        }

        public virtual async Task<T> GetByIdAsync(Guid id)
        {
            try
            {
                return await _snifferDbContext.Set<T>().FindAsync(id);
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Exception in {nameof(GetByIdAsync)}");
                Console.WriteLine(e);
                Console.WriteLine("Try to get again");
                var res = await GetByIdAsync(id);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Success get from db");
                Console.ForegroundColor = ConsoleColor.White;

                return res;
            }
        }

        public virtual async Task InsertAsync(T entity)
        {
            try
            {
                await _snifferDbContext.Set<T>().AddAsync(entity);
                await _snifferDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Exception in {nameof(InsertAsync)}");
                Console.WriteLine(e);
                Console.WriteLine("Try to insert again");
                await InsertAsync(entity);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Success insert to db");
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

        public virtual async Task InsertAsync(ICollection<T> entities)
        {
            try
            {
                await _snifferDbContext.Set<T>().AddRangeAsync(entities);
                await _snifferDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Exception in {nameof(InsertAsync)}");
                Console.WriteLine(e);
                Console.WriteLine("Try to insert again");
                await InsertAsync(entities);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Success insert to db");
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

        public virtual async Task DeleteAsync(T entity)
        {
            try
            {
                _snifferDbContext.Set<T>().Remove(entity);
                await _snifferDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Exception in {nameof(InsertAsync)}");
                Console.WriteLine(e);
                Console.WriteLine("Try to delete again");
                await DeleteAsync(entity);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Success delete from db");
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

        public IQueryable<T> Table => _snifferDbContext.Set<T>();
    }
}
