﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoyoSniffer.Repositories.Abstract
{
    public interface IRepository<T>
    {
        Task<T> GetByIdAsync(Guid id);
        Task InsertAsync(T entity);
        Task InsertAsync(ICollection<T> entities);
        Task DeleteAsync(T entity);
        IQueryable<T> Table { get; }
    }
}
