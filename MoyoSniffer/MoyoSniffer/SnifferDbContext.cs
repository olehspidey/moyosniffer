﻿using Microsoft.EntityFrameworkCore;
using MoyoSniffer.Models;

namespace MoyoSniffer
{
    public class SnifferDbContext : DbContext
    {
        public SnifferDbContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<Product> Products { get; set; }

        public DbSet<SuccessProduct> SuccessProducts { get; set; }

        public DbSet<Route> Routes { get; set; }
    }
}
