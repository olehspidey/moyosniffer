﻿using MoyoSniffer.Models.Abstract;

namespace MoyoSniffer.Models
{
    public class Route : Entity
    {
        public string Url { get; set; }

        public double Price { get; set; }
    }
}
