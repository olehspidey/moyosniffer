﻿using MoyoSniffer.Models.Abstract;

namespace MoyoSniffer.Models
{
    public class Product : Entity
    {
        private double _discount;

        public string Url { get; set; }

        public double Price { get; set; }

        public double? PriceWithDiscount { get; set; }

        public double Discount
        {
            get
            {
                var discount = (100 - (PriceWithDiscount / Price ?? 0) * 100);

                if ((int)discount == 100)
                    return 0;

                return discount;
            }
            set => _discount = value;
        }

        public string Name { get; set; }
    }
}
