﻿using MoyoSniffer.Models.Abstract;

namespace MoyoSniffer.Models
{
    public class SuccessProduct : Entity
    {
        public string Url { get; set; }

        public double Discount { get; set; }
    }
}
