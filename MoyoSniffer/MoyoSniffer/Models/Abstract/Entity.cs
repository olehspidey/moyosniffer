﻿using System;

namespace MoyoSniffer.Models.Abstract
{
    public class Entity
    {
        public Guid Id { get; set; }

        public Entity()
        {
            Id = Guid.NewGuid();
        }
    }
}
