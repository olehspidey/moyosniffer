﻿using System.Collections.Generic;
using MoyoSniffer.Models;

namespace MoyoSniffer
{
    public class RouteComparer : IEqualityComparer<Route>
    {
        public bool Equals(Route x, Route y) => x.Url == y.Url;

        public int GetHashCode(Route obj) => obj.Id.GetHashCode() ^ obj.Url.GetHashCode() ^ obj.Price.GetHashCode();
    }
}
