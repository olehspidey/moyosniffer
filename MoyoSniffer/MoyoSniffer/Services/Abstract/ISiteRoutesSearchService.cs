﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MoyoSniffer.Models;

namespace MoyoSniffer.Services.Abstract
{
    public interface ISiteRoutesSearchService
    {
        Task<IList<string>> SearchRoutesFromMenuAsync();
        Task<IList<Route>> SearchRoutesFromProductListAsync(int priceFrom = 0, int priceTo = int.MaxValue);
        Task SaveChankToDbAsync(ICollection<Route> routes);
    }
}
