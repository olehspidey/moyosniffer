﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MoyoSniffer.Models;

namespace MoyoSniffer.Services.Abstract
{
    public interface IProductSearcherService
    {
        Task<Product> SearchProductAsync(string url);
        Task<IList<Product>> SearchProductsAsync(IList<Route> routes);
        Task SaveProductsChankToDbAsync(ICollection<Product> products);
    }
}
