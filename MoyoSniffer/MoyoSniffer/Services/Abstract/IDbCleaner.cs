﻿using System.Threading.Tasks;

namespace MoyoSniffer.Services.Abstract
{
    public interface IDbCleaner
    {
        Task ClearNonUniqueValuesAsync();
    }
}
