﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MoyoSniffer.Services.Abstract
{
    public interface IJsonReadService
    {
        Task<List<T>> ReadArrayAsync<T>(string path);
    }
}
