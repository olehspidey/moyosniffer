﻿using System;

namespace MoyoSniffer.Services.Abstract
{
    public interface IBenchmarkTimer
    {
        DateTime OldTime { get; }

        TimeSpan Tick { get; }
    }
}
