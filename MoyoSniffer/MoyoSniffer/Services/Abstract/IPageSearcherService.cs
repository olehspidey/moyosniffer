﻿using System.Threading.Tasks;

namespace MoyoSniffer.Services.Abstract
{
    public interface IPageSearcherService
    {
        Task<int> SearchTotalPages(string url, int perPage = 96);
    }
}
