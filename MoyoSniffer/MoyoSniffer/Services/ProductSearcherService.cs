﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Html.Dom;
using MoyoSniffer.Models;
using MoyoSniffer.Repositories;
using MoyoSniffer.Repositories.Abstract;
using MoyoSniffer.Services.Abstract;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;

namespace MoyoSniffer.Services
{
    public class ProductSearcherService : IProductSearcherService
    {
        private readonly IBrowsingContext _browsingContext;
        private readonly IBenchmarkTimer _benchmarkTimer;
        private readonly IConfiguration _configuration;
        private readonly ProductRepository _productRepository;
        private readonly IRepository<SuccessProduct> _successProductRepository;
        private readonly int _chankSize = 50;

        public ProductSearcherService(IBrowsingContext browsingContext,
            IBenchmarkTimer benchmarkTimer,
            IConfiguration configuration,
            ProductRepository productRepository,
            IRepository<SuccessProduct> successProductRepository)
        {
            _browsingContext = browsingContext;
            _benchmarkTimer = benchmarkTimer;
            _configuration = configuration;
            _productRepository = productRepository;
            _successProductRepository = successProductRepository;
            _chankSize = int.TryParse(configuration["DBSaving:ChankSize"], out var chankSize) ? chankSize : _chankSize;
        }

        public async Task<Product> SearchProductAsync(string url)
        {
            if (string.IsNullOrWhiteSpace(url))
                throw new ArgumentException(nameof(url));

            var document = await _browsingContext.OpenWithErrorHandlerAsync(url);
            var priceMeta = document.QuerySelector<IHtmlMetaElement>("meta[itemprop='price']");
            var oldPriceDiv = document.QuerySelector<IHtmlDivElement>(".old_price");
            var productName = document.QuerySelector<IHtmlHeadingElement>(".tovar_title__name");
            var product = CreateProduct(priceMeta, oldPriceDiv, productName, url);

            Console.WriteLine($"Tick: {_benchmarkTimer.Tick}");

            return product;
        }

        public async Task<IList<Product>> SearchProductsAsync(IList<Route> routes)
        {
            var products = new List<Product>(50);
            var chankProducts = new List<Product>(50);
            var counter = 0;
            var (discountFrom, discountTo) = GetDiscountRange();

            foreach (var route in routes)
            {
                counter++;

                var product = await SearchProductAsync(route.Url);

                if (product.Name == null || double.IsNaN(product.Discount) || double.IsNaN(product.Price))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"Skip product. Url: {product.Url}. Price {product.Price}. Discount {product.Discount}");
                    Console.ForegroundColor = ConsoleColor.White;

                    continue;
                }

                ShowSearchedProduct(product, counter, routes.Count);

                if (discountFrom >= product.Discount && discountTo <= product.Discount)
                {
                    ShowSuccessSearched(product);
                    await SaveSuccessProductToDb(new SuccessProduct
                    {
                        Discount = product.Discount,
                        Id = product.Id
                    });
                }

                products.Add(product);
                chankProducts.Add(product);
                await SaveProductsChankToDbAsync(chankProducts);
            }

            return products;
        }

        public async Task SaveProductsChankToDbAsync(ICollection<Product> products)
        {
            if (products.Count != _chankSize)
                return;

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Saving products chank to db");
            await _productRepository.InsertAsync(products);
            products.Clear();
            Console.WriteLine("Saved products chank to db");
            Console.ForegroundColor = ConsoleColor.White;
        }

        private static void ShowSearchedProduct(Product product, int counter, int routesCount)
        {
            Console.WriteLine($"Searched product. Name: {product.Name}; Discount: {product.Discount};");
            Console.WriteLine($"Product number {counter} of {routesCount}");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine($"Url: {product.Url}");
            Console.ForegroundColor = ConsoleColor.White;
        }

        private static void ShowSuccessSearched(Product product)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("--------------------------");
            Console.WriteLine($"-----Searched product with discount - {product.Discount}. Url - {product.Url}-----");
            Console.WriteLine("--------------------------");
            Console.ForegroundColor = ConsoleColor.White;
        }

        private (int, int) GetDiscountRange()
        {
            if (int.TryParse(_configuration["ProductSearcherFilter:DiscountFrom"], out var discountFrom) &&
               int.TryParse(_configuration["ProductSearcherFilter:DiscountTo"], out var discountTo))
                return (discountFrom, discountTo);

            return (0, 100);
        }

        private static Product CreateProduct(IHtmlMetaElement priceMeta,
            IHtmlDivElement oldPriceDiv,
            IHtmlHeadingElement productNameHead, string url)
        {
            double.TryParse(priceMeta?.Content, out var price);
            double.TryParse(oldPriceDiv?.Attributes.FirstOrDefault(attr => attr.Name == "data-price")?.Value, out var oldPrice);

            return new Product
            {
                Price = (int)oldPrice == 0 ? price : oldPrice,
                PriceWithDiscount = price,
                Name = productNameHead?.TextContent,
                Url = url
            };
        }

        private async Task SaveSuccessProductToDb(SuccessProduct product)
        {
            Console.WriteLine("Saving success product to db");
            await _successProductRepository.InsertAsync(product);
            Console.WriteLine("Saved success product to db");
        }
    }
}
