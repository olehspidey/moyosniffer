﻿using System;
using MoyoSniffer.Services.Abstract;

namespace MoyoSniffer.Services
{
    public class BenchmarkTimer : IBenchmarkTimer
    {
        public DateTime OldTime { get; }

        public BenchmarkTimer()
        {
            OldTime = DateTime.Now;
        }

        public TimeSpan Tick => DateTime.Now - OldTime;
    }
}
