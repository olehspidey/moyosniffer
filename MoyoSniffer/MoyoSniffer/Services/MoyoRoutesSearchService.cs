﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Html.Dom;
using MoyoSniffer.Models;
using MoyoSniffer.Repositories;
using MoyoSniffer.Services.Abstract;
using static System.Console;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;

namespace MoyoSniffer.Services
{
    public class MoyoRoutesSearchService : ISiteRoutesSearchService
    {
        private readonly IBrowsingContext _browsingContext;
        private readonly IPageSearcherService _pageSearcherService;
        private readonly IBenchmarkTimer _benchmarkTimer;
        private readonly IJsonReadService _jsonReadService;
        private readonly RouteRepository _routeRepository;
        private readonly int _chankSize = 50;
        private const string Url = "https://www.moyo.ua";

        public MoyoRoutesSearchService(IBrowsingContext browsingContext,
            IPageSearcherService pageSearcherService,
            IBenchmarkTimer benchmarkTimer,
            IJsonReadService jsonReadService,
            RouteRepository routeRepository,
            IConfiguration configuration)
        {
            _browsingContext = browsingContext;
            _pageSearcherService = pageSearcherService;
            _benchmarkTimer = benchmarkTimer;
            _jsonReadService = jsonReadService;
            _routeRepository = routeRepository;
            _chankSize = int.TryParse(configuration["DBSaving:ChankSize"], out var chankSize) ? chankSize : _chankSize;
        }

        public async Task<IList<string>> SearchRoutesFromMenuAsync()
        {
            WriteLine("Searching router in menu");

            var excludedMenuNames = await _jsonReadService.ReadArrayAsync<string>("excludedCategories.json");
            var routesList = new List<string>();
            var document = await _browsingContext.OpenWithErrorHandlerAsync(Url);
            var htmlCollectionLvl2 = document
                .QuerySelectorAll<IHtmlUnorderedListElement>(".lvl2")
                .Where(element =>
                {
                    var menuName = ((IHtmlListItemElement)element.Parent.Parent).QuerySelector<IHtmlSpanElement>("span").TextContent;

                    return !excludedMenuNames.Contains(menuName);
                })
                .ToList();

            ForegroundColor = ConsoleColor.Green;
            WriteLine($"Searched {htmlCollectionLvl2.Count} main menu routes");
            ForegroundColor = ConsoleColor.Cyan;
            WriteLine($"Excluded {excludedMenuNames.Count} main menu routes");
            ForegroundColor = ConsoleColor.White;

            foreach (var lvl2Item in htmlCollectionLvl2)
            {
                var lvl3div = lvl2Item.QuerySelectorAll<IHtmlDivElement>(".lvl3-wrap").ToList();

                if (!lvl3div.Any())
                {
                    routesList.Add(lvl2Item.QuerySelector<IHtmlAnchorElement>(".menu-item-lvl2").Href);
                    break;
                }

                var lvl3Items = lvl3div
                    .SelectMany(element => element.QuerySelectorAll<IHtmlAnchorElement>(".menu-item-lvl3"),
                        (element, anchorElement) => anchorElement.Href);

                routesList.AddRange(lvl3Items);
            }

            WriteLine($"Searched {routesList.Count} menu routes");

            return routesList;
        }

        public async Task<IList<Route>> SearchRoutesFromProductListAsync(int priceFrom = 0, int priceTo = int.MaxValue)
        {
            var routesFromMenu = await SearchRoutesFromMenuAsync();
            var routes = new List<Route>();
            var routesChank = new List<Route>(50);

            foreach (var route in routesFromMenu)
            {
                WriteLine($"Searching total pages. Tick {_benchmarkTimer.Tick}");
                var totalPageFromRoute = await _pageSearcherService.SearchTotalPages(route);
                WriteLine($"Searched total pages - {totalPageFromRoute}");
                var routesFromPage = await GetRoutesFromPageAsync(totalPageFromRoute, route, true, priceFrom, priceTo);
                routes.AddRange(routesFromPage);
                routesChank.AddRange(routesFromPage);
                await SaveChankToDbAsync(routesChank);

                ForegroundColor = ConsoleColor.Green;
                WriteLine($"Total routes - {routes.Count}");
                ForegroundColor = ConsoleColor.White;
            }

            return routes;
        }

        public async Task SaveChankToDbAsync(ICollection<Route> routes)
        {
            if (routes.Count < _chankSize)
                return;

            ForegroundColor = ConsoleColor.Green;
            WriteLine("Saving routes chank to db");
            await _routeRepository.InsertAsync(routes);
            routes.Clear();
            WriteLine("Chank saved success");
            ForegroundColor = ConsoleColor.White;
        }

        private async Task<IList<Route>> GetRoutesFromPageAsync(int totalPages, string subRoute, bool onlyActive, int priceFrom, int priceTo)
        {
            WriteLine("Searching routes from page");

            var routes = new List<Route>(totalPages);

            for (var i = 1; i <= totalPages; i++)
            {
                var url = $"{subRoute}?page={i}";
                WriteLine($"Searching product routes in {url}. Page = {i}");
                var document = await _browsingContext.OpenWithErrorHandlerAsync(url);

                routes.AddRange(document
                    .QuerySelectorAll<IHtmlDivElement>(".catalog-content .product-tile_inner_wrapper")
                    .Where(element => onlyActive && element.QuerySelector(".product-tile_not-available-text") == null
                                                 && WithPriceDiapasonPredicate(element, priceFrom, priceTo))
                    .Select(element => new Route
                    {
                        Price = int.TryParse(
                            element.QuerySelector<IHtmlSpanElement>(".product-tile_price-value")?.InnerHtml.Replace("&nbsp;", string.Empty),
                            out var price)
                            ? price
                            : 0,
                        Url = element.QuerySelector<IHtmlAnchorElement>(".gtm-link-product")?.Href
                    }));
            }

            return routes;
        }

        private static bool WithPriceDiapasonPredicate(IHtmlDivElement divElement, int priceFrom, int priceTo)
        {
            var priceSpan = divElement.QuerySelector<IHtmlSpanElement>(".product-tile_price-value");

            if (priceSpan == null)
                return false;

            if (int.TryParse(priceSpan.InnerHtml?.Replace("&nbsp;", string.Empty), out var price))
                return price >= priceFrom && price <= priceTo;

            return false;
        }
    }
}
