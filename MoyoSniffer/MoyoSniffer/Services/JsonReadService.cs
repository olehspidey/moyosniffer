﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using MoyoSniffer.Services.Abstract;
using Newtonsoft.Json;

namespace MoyoSniffer.Services
{
    public class JsonReadService : IJsonReadService
    {
        public async Task<List<T>> ReadArrayAsync<T>(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                throw new ArgumentException(nameof(path));

            using (var file = new StreamReader(path, Encoding.UTF8))
            {
                var jsonText = await file.ReadToEndAsync();
                return JsonConvert.DeserializeObject<List<T>>(jsonText);
            }
        }
    }
}
