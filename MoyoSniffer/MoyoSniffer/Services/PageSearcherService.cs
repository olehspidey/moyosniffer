﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Html.Dom;
using MoyoSniffer.Services.Abstract;

namespace MoyoSniffer.Services
{
    public class PageSearcherService : IPageSearcherService
    {
        private readonly IBrowsingContext _browsingContext;
        public PageSearcherService(IBrowsingContext browsingContext)
        {
            _browsingContext = browsingContext;
        }

        public async Task<int> SearchTotalPages(string url, int perPage = 96)
        {
            if (string.IsNullOrWhiteSpace(url))
                throw new ArgumentException(nameof(url));
            
            var document = await _browsingContext.OpenWithErrorHandlerAsync($"{url}?perPage={perPage}");
            var anchorElements = document
                .QuerySelectorAll<IHtmlAnchorElement>(".new-pagination-pages-container .new-pagination-link")
                .ToList();

            if (!anchorElements.Any())
                return 1;

            return anchorElements
                .Max(element => int.Parse(element.Attributes.FirstOrDefault(attr => attr.Name == "data-page").Value));
        }
    }
}
