﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Internal;
using MoyoSniffer.Services.Abstract;

namespace MoyoSniffer.Services
{
    public class DbCleaner : IDbCleaner
    {
        private readonly SnifferDbContext _snifferDbContext;

        public DbCleaner(SnifferDbContext snifferDbContext)
        {
            _snifferDbContext = snifferDbContext;
        }

        public async Task ClearNonUniqueValuesAsync()
        {
            Console.WriteLine("Start to clean db");
            var uniqueRouteIds = _snifferDbContext
                .Routes
                .ToList()
                .Distinct((r1, r2) => r1.Url == r2.Url)
                .Select(route => route.Id)
                .ToList();
            Console.WriteLine($"Searched {uniqueRouteIds.Count} unique routes");

            var uniqueProductIds = _snifferDbContext
                .Products
                .ToList()
                .Distinct((p1, p2) => p1.Url == p2.Url)
                .Select(product => product.Id)
                .ToList();
            Console.WriteLine($"Searched {uniqueProductIds.Count} unique products");
            var routesToDelete = _snifferDbContext
                .Routes
                .ToList()
                .Where(route => !uniqueRouteIds.Contains(route.Id))
                .ToList();
            Console.WriteLine($"Searched {routesToDelete.Count} routes to delete");
            var productsToDelete = _snifferDbContext
                .Products
                .ToList()
                .Where(product => !uniqueProductIds.Contains(product.Id))
                .ToList();
            Console.WriteLine($"Searched {productsToDelete.Count} products to delete");

            if (!routesToDelete.Any() && !productsToDelete.Any())
            {
                Console.WriteLine("All values is unique. Nothing to delete");

                return;
            }

            _snifferDbContext
                .Routes
                .RemoveRange(routesToDelete);
            _snifferDbContext
                .Products
                .RemoveRange(productsToDelete);

            await _snifferDbContext.SaveChangesAsync();
            Console.WriteLine("Deleted non unique values success");
            Console.WriteLine($"Deleted {routesToDelete.Count} routes");
            Console.WriteLine($"Deleted {productsToDelete.Count} products");
        }
    }
}
