﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Io;

namespace MoyoSniffer
{
    public static class BrowsingContextExtensions
    {
        public static async Task<IDocument> OpenWithErrorHandlerAsync(this IBrowsingContext context, string url)
        {
            try
            {
                var documentLoader = context.GetService<IDocumentLoader>();
                var response = await documentLoader.FetchAsync(new DocumentRequest(Url.Create(url))).Task.ConfigureAwait(false);

                response = await ReconnectIfNullResponseAsync(response, documentLoader, url);

                if (response.StatusCode == HttpStatusCode.Forbidden
                    || response.StatusCode == HttpStatusCode.NotFound
                    || response.StatusCode == HttpStatusCode.BadRequest)
                    return await SendRequestWithoutProxyAsync(url);

                Console.WriteLine("****Created connection with proxy****");

                var documentOptions = new CreateDocumentOptions(response, context.GetDefaultEncoding());
                var documentFactory = context.GetFactory<IDocumentFactory>();

                return await documentFactory.CreateAsync(context, documentOptions, CancellationToken.None);
            }
            catch (Exception e)
            {
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Exception was through in {nameof(OpenWithErrorHandlerAsync)}");
                Console.WriteLine();
                Console.WriteLine(e);
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Try to reconnect");

                var reconnectResult = await OpenWithErrorHandlerAsync(context, url);

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Success reconnect");
                Console.ForegroundColor = ConsoleColor.White;
                return reconnectResult;
            }
        }

        private static Task<IDocument> SendRequestWithoutProxyAsync(string url, IBrowsingContext context = null, HttpStatusCode? errorStatusCode = null)
        {
            Console.ForegroundColor = ConsoleColor.Red;

            if (errorStatusCode != null) Console.WriteLine($"Error response. Status code: {errorStatusCode}");

            Console.WriteLine("Tray to send request without proxy");

            var browsingContext = context ?? BrowsingContext.New(Configuration.Default.WithDefaultLoader());
            var document = browsingContext.OpenAsync(url);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nRequest was sent success\n");
            Console.ForegroundColor = ConsoleColor.White;

            return document;
        }

        private static async Task<IResponse> ReconnectIfNullResponseAsync(IResponse response, IDocumentLoader documentLoader, string url)
        {
            while (response == null)
            {
                Console.WriteLine("Try to reconnect");

                response = await documentLoader.FetchAsync(new DocumentRequest(Url.Create(url))).Task.ConfigureAwait(false);

                if (response != null)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Success reconnect");
                    Console.ForegroundColor = ConsoleColor.White;

                    break;
                }
            }

            return response;
        }
    }
}
