﻿using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Io;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MoyoSniffer.Providers;
using MoyoSniffer.Repositories;
using MoyoSniffer.Repositories.Abstract;
using MoyoSniffer.Services;
using MoyoSniffer.Services.Abstract;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;

namespace MoyoSniffer
{
    internal class Program
    {
        public static async Task Main(string[] args)
        {
            var services = CreateServices();

            await AddBrowsingContextAsync(services);
            var config = CreateConfiguration(services);

            AddDbContext(services, config);
            AddRepositories(services);

            var servicesProvider = services.BuildServiceProvider();
            var productSearcher = servicesProvider.GetRequiredService<IProductSearcherService>();
            var routesProvider = servicesProvider.GetRequiredService<IRoutesProvider>();
            var dbCleaner = servicesProvider.GetRequiredService<IDbCleaner>();

            await new SnifferExecutor(routesProvider, productSearcher, dbCleaner)
                .StartAsync(args);
        }

        private static IServiceCollection CreateServices()
            => new ServiceCollection()
                .AddTransient<ISiteRoutesSearchService, MoyoRoutesSearchService>()
                .AddTransient<IPageSearcherService, PageSearcherService>()
                .AddSingleton<IBenchmarkTimer, BenchmarkTimer>(_ => new BenchmarkTimer())
                .AddSingleton<IJsonReadService, JsonReadService>()
                .AddTransient<IProductSearcherService, ProductSearcherService>()
                .AddTransient<IRoutesProvider, RoutesProvider>()
                .AddTransient<IDbCleaner, DbCleaner>();

        private static void AddRepositories(IServiceCollection serviceCollection)
            => serviceCollection
                .AddTransient(typeof(IRepository<>), typeof(Repository<>))
                .AddTransient<ProductRepository>()
                .AddTransient<RouteRepository>();

        private static IConfiguration CreateConfiguration(IServiceCollection collection)
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .AddJsonFile("configuration.json")
                .Build();

            collection.AddSingleton(_ => configuration);

            return configuration;
        }

        private static void AddDbContext(IServiceCollection serviceCollection, IConfiguration configuration)
            => serviceCollection.AddDbContext<SnifferDbContext>(options => options
                .UseSqlServer(configuration.GetConnectionString("ProdCon")));
        private static async Task AddBrowsingContextAsync(IServiceCollection serviceCollection, string path = "proxyList.json")
        {
            var proxyList = await new JsonReadService().ReadArrayAsync<string>(path);
            var proxyBuilder = new ProxyBuilder(proxyList);
            var configuration = Configuration
                .Default
                .With(new DefaultHttpRequester(null, request =>
                {
                    request.Proxy = proxyBuilder.CreateProxyFromList();
                }))
                .WithDefaultLoader();
            serviceCollection.AddTransient(_ => BrowsingContext.New(configuration));
        }
    }
}
